module.exports = {
  siteTitle: 'Sca Dienstleistung GmbH', // <title>
  manifestName: 'Sca Dienstleistung',
  manifestShortName: 'Landing', // max 12 characters
  manifestStartUrl: '/',
  manifestBackgroundColor: '#663399',
  manifestThemeColor: '#663399',
  manifestDisplay: 'standalone',
  manifestIcon: 'src/assets/img/logo.png',
  pathPrefix: `/gatsby-starter-solidstate/`, // This path is subpath of your hosting https://domain/portfolio
  heading: 'Sca Dienstleistung',
  subHeading: 'Ihr zuverlässiger Partner für Abbruch, Entkernung und Schadstoffsanierungen aller Art ',
  // social
  socialLinks: [

    {
      icon: 'fa-facebook',
      name: 'Facebook',
      url: 'https://facebook.com/',
    },
    {
      icon: 'fa-envelope-o',
      name: 'Email',
      url: 'mailto:info@sca-dienstleistung.de',
    },
  ],
  phone: '0176/47828989',
  address: 'Oberhausen, Deutschland',
};
